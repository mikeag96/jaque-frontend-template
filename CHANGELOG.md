# Changelog

All notable changes to this project will be documented in this file. See
[standard-version](https://github.com/conventional-changelog/standard-version)
for commit guidelines.

## [0.2.0](https://gitlab.com/mikeag96/jaque-frontend-template/-/compare/v0.1.0...v0.2.0) (2021-02-22)

### Features

- add container styles
  ([1aa4112](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/1aa4112b7c3aa903c26b38d956340df878683ceb))
- add home page
  ([9cfc1c7](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/9cfc1c79293f4fa597c916b91fe11160965ae869))
- add snippet service
  ([c35fc61](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/c35fc61508ee1843910d504f7c9fcb23636bbfd1))
- add unit test content with highlight in sticky navbar page
  ([121efef](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/121efef64e3d2cfb666f890021fef504d30681c6))
- implements highlight provider and import HttpClientModule
  ([20e195c](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/20e195c69caf4f29de772d4aea2c4c3cea230ca8))
- install ngx-highlight
  ([cd8274f](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/cd8274f574779675b6480c1ebca1f01adb9cbdc2))

### Bug Fixes

- example route for sicky navbar component
  ([cd70585](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/cd70585878050035fc95ecb612af5588fc166eee))

## 0.1.0 (2021-02-19)

### Features

- add commit manager and autogenerate changelog file
  ([1e4b20b](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/1e4b20bc6dd209d8d5792ebb7a0805ce74da2c00))
- add docs for all components
  ([09e8b52](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/09e8b528048e93a7570afcb874cfdd803b00e7a4))
- add documentation folder to gitignore
  ([4336a39](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/4336a39145f215bcc3fd0839cc88940523eee695))
- add scripts to build compodoc documentation
  ([24e3501](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/24e3501dfaa2127654d8f7da3dac5035e47e68b3))
- add sticky navbar component with unit tests
  ([bd33591](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/bd335918c4a6c32f1c135b962d357d58370c41ec))
- create home component files
  ([5b2cc88](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/5b2cc886e9178f2949c89258794ee3fbe93f9632))
- deploy compodoc in gitlab pages
  ([07aa181](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/07aa181119f4fd67b83a06daea427a1fa3135292))
- reset css
  ([251a22e](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/251a22ea20cad2da71f9f6471eb09df5881bb1cd))
- update gitlab ci
  ([0c68c2e](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/0c68c2ea66f60a2858d06b3eb283320053baae92))

### Bug Fixes

- change artifacts folder destination
  ([53b3a27](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/53b3a2720cc46737ff7106ae35a9fbfd7692f364))
- gitlab ci to deploy documentation
  ([e160de9](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/e160de9956590b7c6942c9f8329feecded4cbc1f))

## 0.0.0 (2021-01-14)

### Features

- add commit manager and autogenerate changelog file
  ([9fb48a6](https://gitlab.com/mikeag96/jaque-frontend-template/-/commit/9fb48a6ce36df224bd103f3f4c1cf115cfbd7c35))
