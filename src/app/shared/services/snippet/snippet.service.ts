import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

/**
 * SnippetService
 * Get a snippet from GitLab Snippets
 */
@Injectable({
  providedIn: 'root',
})
/**
 * SnippetService
 */
export class SnippetService {
  /**
   * Constructor
   *
   * @param http Use HttpClient
   */
  constructor(private readonly http: HttpClient) {}

  /**
   * Get a snippet from GitLab API
   *
   * @param id Snippet ID
   */
  public get(id: string): Observable<string> {
    const path = environment.snippetsAPIUrl.replace(':id', id);

    return this.http.get(path, { responseType: 'text' });
  }
}
