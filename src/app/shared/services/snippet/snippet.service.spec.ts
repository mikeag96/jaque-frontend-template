import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { stickyNavbarTestSnippetId } from '@config/snippets.config';

import { SnippetService } from './snippet.service';

describe('SnippetService', () => {
  let service: SnippetService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(SnippetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get snippet with an id', (done: DoneFn) => {
    service.get(stickyNavbarTestSnippetId).subscribe((val) => {
      expect(val).toBeDefined();
      done();
    });
  });
});
