import { Component } from '@angular/core';
/**
 * HomeComponent
 * Includes a example components list with Angular with Unit Tests
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
/**
 * HomeComponent
 * Includes a example components list with Angular with Unit Tests
 */
export class HomeComponent {}
