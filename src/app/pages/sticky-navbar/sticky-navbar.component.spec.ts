import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HighlightModule } from 'ngx-highlightjs';
import { StickyNavbarComponent } from './sticky-navbar.component';

describe('StickyNavbarComponent', () => {
  let component: StickyNavbarComponent;
  let fixture: ComponentFixture<StickyNavbarComponent>;

  let navbar: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StickyNavbarComponent],
      imports: [HttpClientModule, HighlightModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StickyNavbarComponent);
    component = fixture.componentInstance;
    navbar = component.navbar.nativeElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('should exits is-sticky class', () => {
    beforeEach(() => {
      document.body.style.height = '1500px';
      document.documentElement.scrollTop = 51;

      document.dispatchEvent(new Event('scroll'));

      fixture.detectChanges();
    });

    it('should exits is-sticky class in navbar element when scrollTop is major to 50" ', () => {
      expect(navbar.classList.contains('is-sticky')).toBeTruthy();
    });

    it('should remove is-sticky class in navbar element when scrollTop is minor to 51" ', () => {
      expect(navbar.classList.contains('is-sticky')).toBeTruthy();

      document.documentElement.scrollTop = 50;

      document.dispatchEvent(new Event('scroll'));

      fixture.detectChanges();

      expect(navbar.classList.contains('is-sticky')).toBeFalsy();
    });
  });
});
