import { DOCUMENT } from '@angular/common';
import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { SnippetService } from '@shared/services/snippet/snippet.service';
import { stickyNavbarTestSnippetId } from '@config/snippets.config';
/**
 * StickyNavbarComponent
 * <example-url>https://jaque-frontend-template.vercel.app/sticky-navbar</example-url>
 */
@Component({
  selector: 'app-sticky-navbar',
  templateUrl: './sticky-navbar.component.html',
  styleUrls: ['./sticky-navbar.component.scss'],
})
/**
 * StickyNavbarComponent
 * This component is use like a sticky navbar, assign a class according to scroll top
 */
export class StickyNavbarComponent implements OnInit, OnDestroy {
  /**
   * Refers to the element named "navbar"
   */
  @ViewChild('navbar', { static: true }) navbar: ElementRef;

  /**
   * Snippet
   */
  public snippet$: Observable<string>;

  /**
   * Declare a variable for init navbar subscription and destroy subscription in component life cycle
   */
  private stickySubscription = Subscription.EMPTY;

  /**
   *
   * Constructor
   *
   * @param document Inject document for use in Angular
   * @param renderer Declare Renderer2 to access to sticky navbar
   */
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private readonly renderer: Renderer2,
    private readonly snippetService: SnippetService
  ) {}

  /**
   * Initialize navbar listener
   */
  ngOnInit(): void {
    this.listenerDocumentScroll();

    this.snippet$ = this.snippetService.get(stickyNavbarTestSnippetId);
  }

  /**
   * Destroy sticky subscription
   */
  ngOnDestroy(): void {
    this.stickySubscription.unsubscribe();
  }

  /**
   *
   * Scroll listener in document for menu convert to menu sticky
   *
   */
  private listenerDocumentScroll(): void {
    this.stickySubscription = fromEvent(this.document, 'scroll')
      .pipe(filter(() => !!this.document && !!this.document.documentElement))
      .subscribe(() => {
        const { scrollTop } = this.document.documentElement;
        const { nativeElement } = this.navbar;

        if (scrollTop > 50) {
          this.renderer.addClass(nativeElement, 'is-sticky');
        } else {
          this.renderer.removeClass(nativeElement, 'is-sticky');
        }
      });
  }
}
