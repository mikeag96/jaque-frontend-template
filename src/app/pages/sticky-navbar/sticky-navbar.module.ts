import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HighlightModule } from 'ngx-highlightjs';
import { StickyNavbarRoutingModule } from './sticky-navbar-routing.module';
import { StickyNavbarComponent } from './sticky-navbar.component';

@NgModule({
  declarations: [StickyNavbarComponent],
  imports: [CommonModule, StickyNavbarRoutingModule, HighlightModule],
})
export class StickyNavbarModule {}
