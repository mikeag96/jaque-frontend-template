import { Component } from '@angular/core';

/**
 * App Component
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
/**
 * App Component
 * Angular main class
 */
export class AppComponent {}
