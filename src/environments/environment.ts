/**
 * Environment
 * You can add here your environment variables
 */
export const environment = {
  production: false,
  snippetsAPIUrl: 'https://gitlab.com/api/v4/snippets/:id/raw',
};
