export const environment = {
  production: true,
  snippetsAPIUrl: 'https://gitlab.com/api/v4/snippets/:id/raw',
};
