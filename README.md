# Welcome to jaque-frontend-template 👋

![Version](https://img.shields.io/badge/version-0.0.0-blue.svg?cacheSeconds=2592000)
![Prerequisite](https://img.shields.io/badge/npm-%3E%3D6.14.6-blue.svg)
![Prerequisite](https://img.shields.io/badge/node-12.18.3-blue.svg)
[![License: https://opensource.org/licenses/MIT](https://img.shields.io/badge/License-https://opensource.org/licenses/MIT-yellow.svg)](https://opensource.org/licenses/MIT)

> This project contains a hook that validate `lint` project and `prettier` rules
> every run the command `git commit -m '...'`

### 🏠 [Homepage](https://gitlab.com/mikeag96/jaque-frontend-template)

## Prerequisites

- npm >=6.14.6
- node 12.18.3
- Angular Cli >10.0.8

## Install

```shell
// Select the node version
> nvm use
Found '.../jaque-frontend-template/.nvmrc' with version <12.18.3>
Now using node v12.18.3 (npm v6.14.6)
```

```shell
// Install dependencies with npm
> npm ci
⸨          ░░░░░░░░⸩ ⠏ extractTree: sill extract
...
added 1531 packages in 19.488s
```

## Usage

```sh
npm run start
// or
ng s --port 4200
```

## Run tests

```sh
npm run test
```

## Author

👤 **Miguel López**

- Website: https://anxelin.dev/
- Github: [@mikeg96](https://github.com/mikeg96)
- LinkedIn: [@malgarcia](https://linkedin.com/in/malgarcia)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check
[issues page](https://gitlab.com/mikeag96/jaque-frontend-template/-/issues). You
can also take a look at the
[contributing guide](https://gitlab.com/mikeag96/jaque-frontend-template/blob/development/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2020 [Miguel López](https://github.com/mikeg96).

This project is [MIT](https://opensource.org/licenses/MIT) licensed.

---

_This README was generated with ❤️ by
[readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
